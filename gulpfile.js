'use strict';

const
    gulp = require('gulp'),
    gettext = require('gulp-angular-gettext'),
    fs = require('fs'),
    exec = require('child_process').exec,
    jeditor = require("gulp-json-editor"),
    _ = require('lodash');

class gulpTask {

    constructor() {
        const root = __dirname;

        this.params = {
            paths: {
                root: __dirname,
                temp: __dirname + '/temp',
                languages: __dirname + '/src/languages',
                dist: __dirname + '/src/assets'
            },
            locales: {
                "de": {"code": "de", "label": "German", "locale": "de_DE"},
                "en": {"code": "en", "label": "English", "locale": "en_US"},
                "ru": {"code": "ru", "label": "Russian", "locale": "ru_RU"},
                "tr": {"code": "tr", "label": "Turkish", "locale": "tr_TR"},
                "zh-cn": {"code": "zh-cn", "label": "China", "locale": "zh_CN"}
            }
        };
        this.runAllTasks();
    }

    runAllTasks() {
        const tasks = Object.getOwnPropertyNames(this.__proto__);
        if (!tasks) return;

        tasks.forEach(task => {
            if (task.search(/Task$/) !== -1) {
                this[task]();
            }
        });
    }

    execShell(command, cb) {
        exec(command, (err, stdout, stderr) => {
            if (stderr) {
                process.stdout.write('Error:\n' + stderr);
            }
            cb(err);
        });
    };

    makeTranslationsTask() {
        gulp.task('make-pot', () => {
            return gulp.src(['src/app/**/*.html', 'src/app/**/*.ts'])
                .pipe(gettext.extract('languages.pot', {
                    markerNames: ['instant']
                }))
                .pipe(gulp.dest(this.params.paths.temp));
        });

        gulp.task('pot-to-po', (cb) => {

            let command =
                _.keys(this.params.locales).map(locale => {
                    const poFilePath = `${this.params.paths.languages}/${locale}.po`;

                    return (fs.existsSync(poFilePath))
                        ? `msgmerge --force-po --lang=${locale} ${poFilePath} ${this.params.paths.temp}/languages.pot -o ${poFilePath}\n`
                        : `msgcat --force-po --lang=${locale} ${this.params.paths.temp}/languages.pot -o ${poFilePath}\n`;
                }).join('');

            this.execShell(command, cb);
        });

        gulp.task('po-to-json', () => {

            return gulp.src(`${this.params.paths.languages}/*.po`)
                .pipe(gettext.compile({
                    format: 'json'
                }))
                .pipe(jeditor((json) => {
                    const locale = _.keys(json)[0];
                    return json[locale];
                }))
                .pipe(gulp.dest(`${this.params.paths.dist}/i18n`))
        });
    }

    buildTask() {
        if (!fs.existsSync(this.params.paths.temp)) {
            fs.mkdirSync(this.params.paths.temp);
        }
        gulp.task(
            'messages',
            gulp.series(
                'make-pot',
                'pot-to-po',
                'po-to-json'
            )
        );
    }

    devTask() {
        gulp.task('dev', gulp.series('messages'));
    }

    defaultTask() {
        gulp.task('default', gulp.series('messages'));
    }
}

new gulpTask();
