import {NgModule, APP_INITIALIZER} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ApiService} from './services/api/api.service';

export function init_app(apiService: ApiService) {
    return () => apiService.getData();
}

@NgModule({
    imports: [HttpClientModule],
    providers: [
        ApiService,
        {provide: APP_INITIALIZER, useFactory: init_app, deps: [ApiService], multi: true}
    ]
})
export class SiteConfigModule {
}
