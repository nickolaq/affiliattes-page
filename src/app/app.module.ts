import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

//COMPONENTS IMPORT
import {AppComponent} from './app.component';
import {HeaderComponent} from './components/sections/header/header.component';
import {BannerComponent} from './components/sections/banner/banner.component';
import {BenefitsComponent} from './components/sections/benefits/benefits.component';
import {CommissionComponent} from './components/sections/commission/commission.component';
import {DescriptionComponent} from './components/sections/description/description.component';
import {FaqComponent} from './components/sections/faq/faq.component';
import {FooterComponent} from './components/sections/footer/footer.component';
import {LanguageSelectorComponent} from './components/base/language-selector/language-selector.component';
import {LoginButtonComponent} from './components/base/login/login-button/login-button.component';
import {ContactInfoComponent} from './components/base/contact-info/contact-info.component';
import {PaymentSystemComponent} from './components/sections/payment-system/payment-system.component';
import {LogoComponent} from './components/base/logo/logo.component';
import {NavComponent} from './components/base/nav/nav.component';
import {CopyrightComponent} from './components/base/copyright/copyright.component';
import {MerchantsComponent} from './components/base/merchants/merchants.component';

//HTTP IMPORT
import {
    HttpClient,
    HttpClientModule,
    HttpClientJsonpModule,
} from '@angular/common/http';

//OTHER IMPORT
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {InlineSVGModule} from 'ng-inline-svg';
import {LazyLoadDirective} from './directives/lazyload.directive';

//ngx-translate
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

//MATERIAL
import {
    MatExpansionModule,
    MatButtonModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatMenuModule,
} from '@angular/material';

//SiteConfig data
import {ApiService} from './services/api/api.service';
import {SiteConfigModule} from './siteconfig.module';



const MaterialComponents = [
    MatExpansionModule,
    MatButtonModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatMenuModule,
];

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        BannerComponent,
        BenefitsComponent,
        CommissionComponent,
        DescriptionComponent,
        FaqComponent,
        FooterComponent,
        LanguageSelectorComponent,
        LoginButtonComponent,
        ContactInfoComponent,
        PaymentSystemComponent,
        LogoComponent,
        NavComponent,
        CopyrightComponent,
        LazyLoadDirective,
        MerchantsComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        HttpClientJsonpModule,
        InlineSVGModule.forRoot(),
        MaterialComponents,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        FormsModule,
        SiteConfigModule,
    ],
    providers: [
        ApiService,
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
