import {Component, OnInit} from '@angular/core';
import {ApiService} from 'src/app/services/api/api.service';
import {appConfig} from 'src/app/config/appConfig';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'affiliattes-page';
    protected bootstrap: any;

    constructor(
        private apiService: ApiService,
    ) {
    }

    ngOnInit(): void {
        this.bootstrap = this.apiService.appConfig;
    }
}
