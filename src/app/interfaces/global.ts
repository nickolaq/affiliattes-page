export interface IContacts {
    skype: string;
    number: string;
    mail: string;
    socialNetwork: ISocialNetwork;
}

export interface ISocialNetwork {
    facebook: string;
    googlePlus: string;
}

export interface IItemMenu {
    name: string;
    scrollTo?: string;
    href?: string;
}