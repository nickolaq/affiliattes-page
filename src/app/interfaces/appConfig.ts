import {IItemMenu, IContacts} from 'src/app/interfaces/global';

export interface IAppConfig {
    name: string;
    mainUrl: string;
    partnersUrl: string;
    loginUrlBtn: string;
    registerUrlBtn: string;
    contacts: IContacts;
    paymentsList: string[];
    languages: string[];
    defaultLang: string;
    merchants: string[];
    menuConfig: IItemMenu[];
    apiConfig: object;
}
