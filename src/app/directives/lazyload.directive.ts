import {Directive, Input, OnInit, OnDestroy, ElementRef, Renderer2} from '@angular/core';

@Directive({
    selector: '[appLazyLoad]'
})
export class LazyLoadDirective implements OnDestroy, OnInit {
    @Input() direction: string;
    @Input() delay = 0;
    private observer: any;

    constructor(
        private element: ElementRef,
        private render: Renderer2
    ) {
    }
    private unobserve() {
        this.observer.disconnect();
    }
    
    ngOnInit(): void {
        this.render.addClass(this.element.nativeElement, 'visible');
        this.observer = new IntersectionObserver((entries: any[]) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    setTimeout(() => {
                        this.render.removeClass(this.element.nativeElement, 'visible');
                        this.render.addClass(this.element.nativeElement, 'visible_' + this.direction);
                    }, this.delay);
                    this.unobserve();
                }
            });
        });
        this.observer.observe(this.element.nativeElement);
    }
    ngOnDestroy(): void {
        this.unobserve();
    }
}