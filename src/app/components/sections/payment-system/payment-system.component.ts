import {Component, ViewEncapsulation} from '@angular/core';
import {ApiService} from 'src/app/services/api/api.service';

@Component({
    selector: 'app-payment-system',
    templateUrl: './payment-system.component.html',
    styleUrls: ['./payment-system.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class PaymentSystemComponent {
    protected config: any;
    protected paymentList: any = [];

    constructor(
        private apiService: ApiService,
    ) {
        this.config = this.apiService.appConfig;
        if (this.config.paymentsList.length) {
            this.paymentList = this.config.paymentsList;
        } else {
            for (const key in this.config.apiConfig.payment_systems) {
                this.paymentList.push(this.config.apiConfig.payment_systems[(key as any)].Init.toLowerCase());
            }
            this.paymentList = this.paymentList.filter((item, index) => {
                return this.paymentList.indexOf(item) === index;
            });
        }
    }
}
