import {Component} from '@angular/core';
import {appConfig} from 'src/app/config/appConfig';

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.scss']
})
export class BannerComponent {
    public registerUrlBtn: string;
    public siteName: string;

    constructor() {
        this.siteName = appConfig.name;
        this.registerUrlBtn = appConfig.loginUrlBtn;
    }
}
