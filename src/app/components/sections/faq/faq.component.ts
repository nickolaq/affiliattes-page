import {Component, OnInit} from '@angular/core';
import {WordpressService} from 'src/app/services/wp/wordpress.service';

@Component({
    selector: 'app-faq',
    templateUrl: './faq.component.html',
    styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
    protected post: any;

    constructor(private wp: WordpressService) {
    }

    async ngOnInit(): Promise<any> {
        this.post = await this.wp.posts;
        this.post = this.post.filter((el: any) => {
            return el.slug === 'faq';
        });
    }
}
