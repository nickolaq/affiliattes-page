import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-commission',
  templateUrl: './commission.component.html',
  styleUrls: ['./commission.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CommissionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
