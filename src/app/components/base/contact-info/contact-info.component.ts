import { Component, OnInit } from '@angular/core';
import { appConfig } from 'src/app/config/appConfig';

@Component({
  selector: 'app-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss']
})
export class ContactInfoComponent {
  public contacts: any;
  public number: number;
  public mail: string;
  public skype: string;

  constructor() { 
    this.contacts = appConfig.contacts;
    this.skype = this.contacts.skype;
    this.mail = this.contacts.mail;
  }
}
