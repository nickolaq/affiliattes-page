import {Component} from '@angular/core';
import {ApiService} from 'src/app/services/api/api.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-language-selector',
    templateUrl: './language-selector.component.html',
    styleUrls: ['./language-selector.component.scss'],
    host: {'class': 'app-language-selector'}
})
export class LanguageSelectorComponent {

    protected languageList: string[] | object[];
    protected language: object;
    protected selectedLang: string;
    protected config: any;

    constructor(
        private apiService: ApiService,
        private translate: TranslateService,
    ) {
        this.config = this.apiService.appConfig;
        this.languageList = this.config.languages.length ? this.config.languages : this.config.apiConfig.languages;
        this.translate.setDefaultLang(this.config.defaultLang);
    }

    protected changeLang(lang: string): void {
        this.translate.use(lang);
    }
}
