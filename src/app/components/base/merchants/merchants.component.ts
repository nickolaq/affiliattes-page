import {Component, OnInit} from '@angular/core';
import {ApiService} from 'src/app/services/api/api.service';

@Component({
    selector: 'app-merchants',
    templateUrl: './merchants.component.html',
    styleUrls: ['./merchants.component.scss']
})
export class MerchantsComponent {
    protected config: any;

    constructor(
        private apiService: ApiService,
    ) {
        this.config = this.apiService.appConfig;
        console.log(this.config);
    }

}
