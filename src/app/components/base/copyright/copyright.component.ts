import {Component} from '@angular/core';
import {appConfig} from 'src/app/config/appConfig';

@Component({
  selector: 'app-copyright',
  templateUrl: './copyright.component.html',
  styleUrls: ['./copyright.component.scss']
})
export class CopyrightComponent {
  public siteName: string;

  constructor() {
    this.siteName = appConfig.name;
  }
}
