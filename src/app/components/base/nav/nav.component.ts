import {Component, ViewEncapsulation} from '@angular/core';
import {appConfig} from 'src/app/config/appConfig';
import {IItemMenu} from 'src/app/interfaces/global';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss'],
    encapsulation: ViewEncapsulation.None
})

export class NavComponent {
    protected menuList: IItemMenu[] = appConfig.menuConfig;

    public scrollToElement(el: string): void {
        const element = document.querySelector(el);
        element.scrollIntoView();
    }
}
