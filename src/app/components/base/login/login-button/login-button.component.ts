import { Component, OnInit } from '@angular/core';
import { appConfig } from 'src/app/config/appConfig';

@Component({
  selector: 'app-login-button',
  templateUrl: './login-button.component.html',
  styleUrls: ['./login-button.component.scss']
})
export class LoginButtonComponent {
  public loginUrnBtn: string;
  public registerUrlBtn: string;

  constructor() { 
    this.loginUrnBtn = appConfig.loginUrlBtn;
    this.registerUrlBtn = appConfig.registerUrlBtn;
  }
}
