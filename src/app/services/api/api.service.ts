import {Injectable} from '@angular/core';
import {appConfig} from 'src/app/config/appConfig';
import {HttpClient} from '@angular/common/http';
import {IAppConfig} from 'src/app/interfaces/appConfig'

import {extend as _extend} from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    private siteConfig: IAppConfig = appConfig;

    //private urlSite: string = appConfig.mainUrl;
    //protected apiUrl: any = `${this.urlSite}/api/v1/bootstrap`;

    constructor(private http: HttpClient) {
    }

    get appConfig() {
        return this.siteConfig;
    }

    public getData(): Promise<any> {
        return this.http.get('/assets/bootstrap.json').toPromise().then((data: any) => {
            this.siteConfig.apiConfig = _extend({}, data.siteconfig);
        });
    }
}
