import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {appConfig} from 'src/app/config/appConfig';

@Injectable({
    providedIn: 'root'
})
export class WordpressService {

    protected urlSite: string = appConfig.mainUrl;
    protected apiUrl: any = `${this.urlSite}/content/wp-json/wp/v2/posts`;

    constructor(private http: HttpClient) {
    }

    public wpApi(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.jsonp(this.apiUrl, '_jsonp').toPromise().then(resolve, reject);
        });
    }

    get posts() {
        return this.wpApi();
    }
}
