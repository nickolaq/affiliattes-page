import {IAppConfig} from 'src/app/interfaces/appConfig';

export const appConfig: IAppConfig = {
    name: 'Example Sakhankov',
    mainUrl: 'https://google.com',
    partnersUrl: 'https://partners.google.com',
    loginUrlBtn: 'https://partners.google.com',
    registerUrlBtn: 'https://partners.google.com',
    contacts: {
        skype: 'some@skype',
        number: '8800353535',
        mail: 'google@google.com',
        socialNetwork: {
            facebook: 'someUrl',
            googlePlus: 'someInfo'
        }
    },
    paymentsList: [],
    languages: ['en', 'ru'],
    defaultLang: 'en',  //en, ru, de
    merchants: [],
    menuConfig: [
        {
            name: 'Why us',
            scrollTo: '#benefits'
        },
        {
            name: 'Commission',
            scrollTo: '#commission'
        },
        {
            name: 'Faq',
            scrollTo: '#faq'
        },
        {
            name: 'About us',
            scrollTo: '#about'
        },
        {
            name: 'T&C',
            scrollTo: '',
            href: ''
        }
    ],
    apiConfig: {}
};
